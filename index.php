<?php
    include "class.php";
    include "db.php";
    include "query.php";
?>
<html>
    <head>
        <title> object oriented php</title>

    </head>
    <body>
    <p> 
        <?php
            $text = 'hello world';
            echo "$text and the universe";
            echo '<br>';
            $msg = new message();
            echo '<br>';
            echo message::$count;
            //echo $msg->text; 
            $msg->show();
            $msg1 = new message("A new text");
            $msg1->show();
            $msg2 = new message();
            $msg2->show();
            echo '<br>';
            echo message::$count;
            echo '<br>';
            $msg3 = new redMessage('A red message');
            $msg3->show();
            echo '<br>';
            $msg4 = new coloredMessage('A colored message');
            $msg4->color = 'green';
            $msg4-> show();
            //$msg4->setcolor('red');
            echo '<br>';
            // $msg5 = new fonts('font message');
            // $msg5-> font= 56; 
            // $msg5-> view();
            echo '<br>';
            // database connection
            $db = new DB('localhost', 'intro','root', '' );
            $dbc = $db->connect();
            $query= new Query($dbc);
            $q = "SELECT * FROM users";
            $result = $query->query($q);
            echo '<br>';
            // echo $result->num_rows;
            if($result->num_rows > 0){
                echo '<table>';
                echo '<tr><th>Name</th><th>Email</th></tr>';
                while($row =$result->fetch_assoc()){
                    echo '<tr>';
                    echo '<td>',$row['name'],'</td>';
                    echo '<td>',$row['email'],'</td>';
                    echo '</tr>';
                }
                echo '</table>';

            } else{
                 echo "sorry, no results";
            }






        ?>
        </p>
    </body>
</html>